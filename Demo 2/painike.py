import RPi.GPIO as GPIO
import time

LED=12
PAINIKE=5
LIIKE=27

GPIO.setmode (GPIO.BCM)
GPIO.setup (LED, GPIO.OUT)
GPIO.setup (LIIKE, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup (PAINIKE, GPIO.IN, pull_up_down=GPIO.PUD_UP)

timer = time.time() + 20

while timer > time.time():
        state = GPIO.input(PAINIKE)
        if GPIO.input(LIIKE):
                print "i see you!"
        else:
                print "i cant see you..."
        if state == False:
                GPIO.output(LED, 1)
        else:
                GPIO.output(LED, 0)
        time.sleep(0.2)

GPIO.cleanup ()
