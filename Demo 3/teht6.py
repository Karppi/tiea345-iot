import Adafruit_DHT as Ada

pin = 4
sensor = Ada.DHT11

hum, temp = Ada.read_retry(sensor,pin)

if hum is not None and temp is not None:
    print("Temp={0:0.1f}*C Hum={1:0.1f}%".format(temp, hum))
else:
    print "lol not works"
    
