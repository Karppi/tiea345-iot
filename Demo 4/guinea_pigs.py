# -*- coding: utf-8 -*-
import numpy as np
import cv2
from matplotlib import pyplot as plt

img1 = cv2.imread("marsu1.jpg",0)
img2 = cv2.imread("hamster.jpg",0)

orb =cv2.ORB_create()

kp = orb.detect(img1,None)
kp, ds = orb.compute(img1, kp)

kp2 = orb.detect(img2,None)
kp2, ds2 = orb.compute(img2, kp2)

#tallentaa uuden kuvan jossa näkyy keypointsit!
#img2 = cv2.drawKeypoints(img1,kp,None,(255,0,0),4)
#plt.imshow(img2),plt.savefig("lol.png")

bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

matches = bf.knnMatch(ds,ds2,1)

img3 = cv2.drawMatchesKnn(img1,kp,img2,kp2,matches,None,flags=2)
plt.imshow(img3),plt.imsave("erot.png", img3)




