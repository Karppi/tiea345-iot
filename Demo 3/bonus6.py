# -*- coding: utf-8 -*-
import SimpleHTTPServer
import SocketServer as socketserver
import os
import threading
import re
import glob

#malli: http://stackoverflow.com/questions/20423285/python-server-capable-of-displaying-an-image-on-a-browser
class MyHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "image/jpg")
        self.end_headers()

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "image/jpg")
        self.end_headers()
        pictures = []
        #glob käy läpi kaikki annettua mallia vastaavat tiedostot, joista rexexpillä karsitaan ylimääräiset
        for picture in glob.glob("nettikuvat/*.jpg"):
            if prog.match(picture):
                pictures.append(picture) #listaan
        pictures.sort() #järjestetään lista vanhimmasta uusimpaan
        #print pictures
        f = open(pictures.pop(), 'rb')
        self.wfile.write(f.read())
        f.close()         

class MyServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    def __init__(self, server_adress, RequestHandlerClass):
        self.allow_reuse_address = True
        socketserver.TCPServer.__init__(self, server_adress, RequestHandlerClass, False)

if __name__ == "__main__":
    prog = re.compile("nettikuvat/\d{4}-\d{2}-\d{2}-\d{2}\.jpg")
    HOST, PORT = "localhost", 1234
    server = MyServer((HOST, PORT), MyHandler)
    server.server_bind()
    server.server_activate()
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()
