# -*- coding: utf-8 -*-
import picamera
import RPi.GPIO as GPIO
import time

pir = 18

GPIO.setmode(GPIO.BCM)
GPIO.setup(pir, GPIO.IN)
#kamera kuntoon
camera = picamera.PiCamera()
camera.framerate = 24
camera.resolution = (1280, 720)

recording = False
timer = -1
while True:
        if GPIO.input(pir):
            #jos liiketta
            timer = -1 #jos oli jo kytketty 15sek aikaa kuvaamaan, niin tama aiheuttaa
            #sen, etta liikkeen kadottua lisataan uudet 15sek viela
            if not recording:
                #jos ei oltu viela aloitettu nauhoittamaan, niin aloitetaan nyt
                recording = True
                dateAndTime = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
                camera.capture(dateAndTime + ".jpg") #kuva
                camera.start_recording(dateAndTime + ".h264") #aikaleima
                #print "recording started"
        else:
            #jos liiketta ei ole
            if recording and timer < 0:
                #kuvaus on paalla ja nyt katkesi ekan kerran liike tunnistumisesta
                #aloitetaan timeri jotta kamera kuvaa viela 15sek
                timer = time.time() + 15
                #print "liikettä ei havaita. kuvataan 15sek vielä"
            elif recording and timer <= time.time():
                #kamera on kuvannut 15sek ylimaaraista, sammutetaan
                camera.stop_recording()
                recording = False
                print "recording completed"
            elif recording:
                print "aika: ", str(timer - time.time())

GPIO.cleanup()
