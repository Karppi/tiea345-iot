import numpy as np
import cv2
import picamera
import time
from matplotlib import pyplot as plt

#mallia napsittu luennon linkeista
face = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_default.xml")
camera = picamera.PiCamera()
print "get ready for picture in..."
for x in range(5,0,-1):
    print x
    time.sleep(1)

print "say cheese!"
camera.capture("face.jpg")
print "*klik*"

image = cv2.imread("face.jpg")

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

faces = face.detectMultiScale(gray,1.3,5)

for (x,y,w,h) in faces:
    image = cv2.rectangle(image, (x,y),(x+w,y+h),(255,0,0),2)
    #roi_gray = gray[y:y+h,x:x+w]
    #roi_color = image[y:y+h,x:x+w]
    #eyes = eye.detectMultiScale(roi_gray)
    #for (ex,ey,ew,eh) in eyes:
    #   cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)


cv2.imshow("img",image),plt.imsave("kasvot_ja_silmat.png", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
