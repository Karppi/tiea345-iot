//
//  How to access GPIO registers from C-code on the Raspberry-Pi
//  Example program
//  15-January-2012
//  Dom and Gert
//  Revised: 15-Feb-2013
 
// Original source: http://elinux.org/RPi_GPIO_Code_Samples#Direct_register_access
// Modified for Raspberry Pi 2 by Tuomas Tenkanen <tuomas.s.tenkanen@jyu.fi>
 
// Access from ARM Running Linux
 
#define BCM2708_PERI_BASE        0x3f000000 // TST: changed for Pi 2, check version
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */
 
 
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>
 
#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)
 
int  mem_fd;
void *gpio_map;
 
// I/O access
volatile unsigned *gpio;
 
 
// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))
 
#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0
 
#define GET_GPIO(g) (*(gpio+13)&(1<<g)) // 0 if LOW, (1<<g) if HIGH
 
#define GPIO_PULL *(gpio+37) // Pull up/pull down
#define GPIO_PULLCLK0 *(gpio+38) // Pull up/pull down clock
 
void setup_io();
  
int main(int argc, char **argv)
{
  int g,rep;
 
  // Set up gpi pointer for direct register access
  setup_io();
 
 /************************************************************************\
  * You are about to change the GPIO settings of your computer.          *
  * Mess this up and it will stop working!                               *
  * It might be a good idea to 'sync' before running this program        *
  * so at least you still have your code changes written to the SD-card! *
 \************************************************************************/
 

int kavelijaVihr = 6;
int kavelijaKelt = 26;
int kavelijaPuna = 17;

int autoVihr = 19;
int autoKelt = 16;
int autoPuna = 13;

int pir = 18;
int nappi = 21;

//kavelijat
INP_GPIO(kavelijaVihr);
OUT_GPIO(kavelijaVihr);
INP_GPIO(kavelijaKelt);
OUT_GPIO(kavelijaKelt);
INP_GPIO(kavelijaPuna);
OUT_GPIO(kavelijaPuna);

//autot
INP_GPIO(autoVihr);
OUT_GPIO(autoVihr);
INP_GPIO(autoKelt);
OUT_GPIO(autoKelt);
INP_GPIO(autoPuna);
OUT_GPIO(autoPuna);

//nappi ja pir
INP_GPIO(pir);
INP_GPIO(nappi);

//valot alkutilaan
GPIO_CLR = 1<<kavelijaVihr;
GPIO_CLR = 1<<kavelijaKelt;
GPIO_CLR = 1<<autoKelt;
GPIO_CLR = 1<<autoPuna;
GPIO_SET = 1<<autoVihr;
GPIO_SET = 1<<kavelijaPuna;

int tila = 0;
int aika = 0;

  for (;;) {	
     switch(tila) {

   	case 0  :
//vihreat autoille, punaiset jalankulkijoille. ei painettu mitaan!
	GPIO_CLR = 1<<kavelijaVihr;
GPIO_CLR = 1<<kavelijaKelt;
GPIO_CLR = 1<<autoKelt;
GPIO_CLR = 1<<autoPuna;

GPIO_SET = 1<<autoVihr;
GPIO_SET = 1<<kavelijaPuna;
	if (GET_GPIO(pir) && GET_GPIO(nappi)){
aika = time(0) + 7;
tila = 1;
} else if (!GET_GPIO(pir) && GET_GPIO(nappi)){
tila = 2;
}
      	break;
	case 1  :
	//nappi painettu mutta autoja tulee!
GPIO_CLR = 1<<kavelijaVihr;
GPIO_CLR = 1<<autoPuna;
GPIO_CLR = 1<<autoKelt;

GPIO_SET = 1<<autoVihr;
GPIO_SET = 1<<kavelijaPuna;
GPIO_SET = 1<<kavelijaKelt;
	if (time(0) > aika || !GET_GPIO(pir)){
	aika = 0;
tila = 2;
	}
        break;
	case 2  :
	//keltaista autoille!
	GPIO_CLR = 1<<kavelijaVihr;
GPIO_CLR = 1<<autoVihr;
GPIO_CLR = 1<<autoPuna;

GPIO_SET = 1<<autoKelt;
GPIO_SET = 1<<kavelijaPuna;
sleep(2);
tila = 3;
        break;
	case 3  :
//autoilla punainen! myos kavelijoilla! odotetaan hetki!
GPIO_CLR = 1<<kavelijaVihr;
GPIO_CLR = 1<<autoVihr;
GPIO_CLR = 1<<autoKelt;

GPIO_SET = 1<<autoPuna;
GPIO_SET = 1<<kavelijaPuna;
sleep(1);
tila = 4;
        break;
	case 4  :
//vihrea kavelijoille! punainen autoille!
GPIO_CLR = 1<<kavelijaPuna;
GPIO_CLR = 1<<autoVihr;
GPIO_CLR = 1<<autoKelt;
GPIO_CLR = 1<<kavelijaKelt;

GPIO_SET = 1<<autoPuna;
GPIO_SET = 1<<kavelijaVihr;
sleep(6);
tila = 5;
aika = time(0) + 5;
        break; 
case 5  :
//vilkutetaan vihreaa!
GPIO_CLR = 1<<kavelijaPuna;
GPIO_CLR = 1<<autoVihr;
GPIO_CLR = 1<<autoKelt;
GPIO_CLR = 1<<kavelijaKelt;

GPIO_SET = 1<<autoPuna;
sleep(1);
if (time(0) < aika){
if (GET_GPIO(kavelijaVihr)){
	GPIO_CLR = 1<<kavelijaVihr;
break;
} else {
GPIO_SET = 1<<kavelijaVihr;
break;
}
} else {
tila = 6;
aika = 0;
}
break;
case 6  :
//punainen kavelijoille ja 2sek paasta keltainen autoille ja sitten tila 0
GPIO_CLR = 1<<kavelijaPuna;
GPIO_CLR = 1<<autoVihr;
GPIO_CLR = 1<<kavelijaKelt;
GPIO_CLR = 1<<kavelijaVihr;

GPIO_SET = 1<<kavelijaPuna;
sleep(2);
GPIO_SET = 1<<autoPuna;
GPIO_SET = 1<<autoKelt;
sleep(1);
tila = 0;
break;
   	default : 
   	break;
}
  }
 
  return 0;
 
} // main
 
 


//
// Set up a memory regions to access GPIO
//
void setup_io()
{
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
   }
 
   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      BLOCK_SIZE,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      GPIO_BASE         //Offset to GPIO peripheral
   );
 
   close(mem_fd); //No need to keep mem_fd open after mmap
 
   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", (int)gpio_map);//errno also set!
      exit(-1);
   }
 
   // Always use volatile pointer!
   gpio = (volatile unsigned *)gpio_map;
 
 
} // setup_io