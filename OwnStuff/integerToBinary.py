import RPi.GPIO as GPIO
import time

leds = [19,25,20,21,26,13]
pots = [64,32,16,8,4,2,1] #ledeja vain 6 joten siksi rajana 64

def setLights(binary):
    for i in range(0,6):
        GPIO.output(leds[i],binary[i])

def intToBin(number):
    binary = []
    for pot in pots:
        if pot <= number:
            number = number - pot
            binary.append(1)
        else:
            binary.append(0)
    binary.reverse()
    return binary
#alustetaan laite kuntoon
GPIO.setmode(GPIO.BCM)
for led in leds:
    GPIO.setup(led, GPIO.OUT)

number = 0

while number < 64 and number >= 0:
    setLights(intToBin(number))
    #oletetaan ja uskotaan etta kayttaja antaa kiltisti oikeita arvoja
    #numeroarvot 0-63 ulkopuolelta sammuttaa ohjelman
    number = int(raw_input("Give me some numbers between 0 and 64: "))
    
GPIO.cleanup ()


