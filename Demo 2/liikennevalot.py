import time
import RPi.GPIO as GPIO

#luodaan enumit pythoniin
def enum(**colors):
    return type("Enum", (), colors)
#valoluokka. Tietaa pinnin numeron ja onko paalla vai ei
class Light:
    power = 0 #0 = poissa paalta, 1 = paalla
    def __init__(self, color, led):
        self.color = color
        self.ledNumber = led
    
    def switch(self):
        if (self.power is 1):
            self.power = 0
        else:
            self.power = 1
        GPIO.output(self.ledNumber, self.power)

    def setPower(self, power):
        self.power = power
        GPIO.output(self.ledNumber, self.power)

class TrafficLight:
    lights = [] #lista valoista
    def __init__(self):
        pass
    
    def addLight(self, light):
        self.lights.append(light) #lisaa valon

    def setLightPower(self, color, power):
        for light in self.lights:
            if light.color is color:
                light.setPower(power)
                break
#autojen liikennevalot.
class CarTrafficLight(TrafficLight):
    def __init__(self):
        pass

    def start(self):
        self.g = GreenState(self) #pysyy ikuisesti
        self.gty = GreenToYellowState(self) #keltainen + viive punaiseen
        self.r = RedState(self) #pysyy ikuisesti
        self.rtg = RedToGreenState(self) #pun+kelt + viive vihreaan
        self.activeState = self.g
        self.activeState.reset()

    def update(self):
        self.activeState.update()

    def changeActiveState(self, state):
        state.reset()
        self.activeState = state

#carstate on tuo state autojen valoille. alla "interface/abstrakti" luokka (en osaa pythonia)
#jonka jalkeen tulee toteutuksia. Komentelevat autojen valoja
class CarState:
    def __init__(self, tl):
        self.tl = tl

    def update(self):
        pass

    def reset(self):
        pass

class GreenState(CarState):
    def __init__(self, tl):
        self.tl = tl

    def update(self):
        pass

    def reset(self):
        self.tl.setLightPower(Colors.GREEN, 1)
        self.tl.setLightPower(Colors.YELLOW, 0)
        self.tl.setLightPower(Colors.RED, 0)

class GreenToYellowState(CarState):
    def __init__(self, tl):
        self.tl = tl
        self.time = time.time() + 2

    def update(self):
        if (self.time < time.time()):
            self.tl.changeActiveState(self.tl.r)

    def reset(self):
        self.time = time.time() + 2
        self.tl.setLightPower(Colors.GREEN, 0)
        self.tl.setLightPower(Colors.YELLOW, 1)
        self.tl.setLightPower(Colors.RED, 0)

class RedState(CarState):
    def __init__(self, tl):
        self.tl = tl
        self.time = time.time() + 2

    def update(self):
        if (self.time < time.time()):
            self.tl.changeActiveState(self.tl.rtg)

    def reset(self):
        self.time = time.time() + 15
        self.tl.setLightPower(Colors.GREEN, 0)
        self.tl.setLightPower(Colors.YELLOW, 0)
        self.tl.setLightPower(Colors.RED, 1)

class RedToGreenState(CarState):
    def __init__(self, tl):
        self.tl = tl
        self.time = time.time() + 2

    def update(self):
        if (self.time < time.time()):
            self.tl.changeActiveState(self.tl.g)
            

    def reset(self):
        self.time = time.time() + 2
        self.tl.setLightPower(Colors.GREEN, 0)
        self.tl.setLightPower(Colors.YELLOW, 1)
        self.tl.setLightPower(Colors.RED, 1)

class ChangerSwitch:
    def __init__(self, pir, ctl, walkerGreen, walkerYellow, walkerRed):
        self.pressed = False #onko painettu
        self.canChange = 0 #0 ei liiketta, 1 on liiketta
        self.changing = False #ollaanko vaihtumassa
        self.timer = 0 #ajastin napille
        self.pir = pir
        self.ctl = ctl
        self.walkerGreen = walkerGreen
        self.walkerYellow = walkerYellow
        self.walkerRed = walkerRed
        GPIO.output(self.walkerRed, 1)
        GPIO.output(self.walkerYellow, 0)
        GPIO.output(self.walkerGreen, 0)

    def update(self):
        if (self.changing is True):
            #riippuen mika tila, niin tietyt varit paalla
            if (self.ctl.activeState is self.ctl.r):
                GPIO.output(self.walkerYellow, 0)
                GPIO.output(self.walkerGreen, 1)
                GPIO.output(self.walkerRed, 0)
            if (self.ctl.activeState is self.ctl.rtg):
                GPIO.output(self.walkerGreen, 0)
                GPIO.output(self.walkerRed, 1)
            if (self.ctl.activeState is self.ctl.g):
                self.changing = False
            return
        if (self.pressed is True):
            #on painettu jalankulkijan toimesta nappia
            self.canChange = GPIO.input(self.pir)
            if (self.canChange is 0):
                self.change()
            elif (self.timer < time.time()):
                self.change()
    #muuttaa tilaa green-to-yellow tilaan
    def change(self):
        self.ctl.changeActiveState(self.ctl.gty)
        self.pressed = False
        self.changing = True
    #napin painalluksesta kutsuttu metodi
    def pressedButton(self):
        #jos vaihdot menossa tai autoilla punainen valo, ei tehda mitaan
        if (self.changing is True or self.ctl.activeState is self.ctl.r):
            return
        if (self.pressed is False):
            self.timer = time.time() + 10 #ajastin jonka kuluessa pakkovaihtuminen
        self.pressed = True
        GPIO.output(self.walkerYellow, 1) #ilmoitusvalo paalle

#luodaan enumeja
Colors = enum(GREEN="green", YELLOW="yellow", RED="red")
#pinninumerot
carRed = 17
carYellow = 6
carGreen = 13

walkerRed = 16
walkerYellow = 20
walkerGreen = 18

button = 21

pir = 25
#tietojen sepustus
GPIO.setmode(GPIO.BCM)
GPIO.setup(carRed, GPIO.OUT)
GPIO.setup(carYellow, GPIO.OUT)
GPIO.setup(carGreen, GPIO.OUT)
GPIO.setup(walkerRed, GPIO.OUT)
GPIO.setup(walkerYellow, GPIO.OUT)
GPIO.setup(walkerGreen, GPIO.OUT)
GPIO.setup(pir, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP) #vastus kooditse
GPIO.setup(button, GPIO.IN) #kun vastus on laudassa HUOM! Muuta if Falseen jos poistat taman
#luodaan autojen liikennevalo ja lisataan valot
ctl = CarTrafficLight()
ctl.addLight(Light(Colors.RED, carRed))
ctl.addLight(Light(Colors.YELLOW, carYellow))
ctl.addLight(Light(Colors.GREEN, carGreen))

switcher = ChangerSwitch(pir, ctl, walkerGreen, walkerYellow, walkerRed)

ctl.start()

timer = time.time() + 90 #ajastin 90sek

while timer > time.time():
    ctl.update()
    switcher.update() #paivitykset
    if (GPIO.input(button) == True):
        switcher.pressedButton()

GPIO.cleanup()
